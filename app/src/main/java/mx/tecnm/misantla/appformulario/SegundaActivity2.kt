package mx.tecnm.misantla.appformulario

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_segunda2.*

class SegundaActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_segunda2)

        val bundle : Bundle? = intent.extras
        bundle?.let {
            val email = it.getString("dato1")
            val name = it.getString("dato2")
            val lastname = it.getString("dato3")
            val pass = it.getString("dato4")

            tvEmail.text = "Email : $email"
            tvNombre.text = "Nombre : $name"
            tvApellido.text = "Apellido : $lastname"
            tvPassword.text = "Password : $pass"
        }

    }
}