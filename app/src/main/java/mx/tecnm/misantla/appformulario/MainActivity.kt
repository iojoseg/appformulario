package mx.tecnm.misantla.appformulario

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRegister.setOnClickListener {

            val email = edtEmail.text.toString()
            val nombre = edtNombre.text.toString()
            val apellido = edtApellido.text.toString()
            val password = edtPassword.text.toString()
            val confirm_pass = edtConfirmPassword.text.toString()


            if(email.isEmpty()){
                Toast.makeText(this,"Debe ingresar un email",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if(nombre.isEmpty()){
                Toast.makeText(this,"Debe ingresar un nombre",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(apellido.isEmpty()){
                Toast.makeText(this,"Debe ingresar un apellido",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if(password.isEmpty()){
                Toast.makeText(this,"Debe ingresar un password",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(confirm_pass.isEmpty()){
                Toast.makeText(this,"Debe ingresar su confirmacion de password",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            val bundle = Bundle()
            bundle.apply {
                putString("dato1",email)
                putString("dato2",nombre)
                putString("dato3",apellido)
                putString("dato4",password)
            }

            val intent = Intent(this,SegundaActivity2::class.java).apply {
               putExtras(bundle)
            }
            startActivity(intent)

           // intent.putExtra("dato1",email)
           /* intent.putExtra("dato2",nombre)
            intent.putExtra("dato3",apellido)
            intent.putExtra("dato4",password)*/



        }
    }
}